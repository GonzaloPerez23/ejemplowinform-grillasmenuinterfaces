﻿using Logica;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ProgramacionWinformGrillas
{
    public partial class FormNuevoPaciente : Form
    {
        public FormNuevoPaciente(Paciente paciente)
        {
            InitializeComponent();
            if (paciente.Id != 0)
            {
                LblTitulo.Text = "Editar paciente";
                LblId.Visible = true;
                LblIdTitle.Visible = true;
                CompletarDatosPaciente(paciente);
            }
            else
            {
                LblTitulo.Text = "Nuevo paciente";
                LblId.Text = "";
                LblId.Visible = false;
                LblIdTitle.Visible = false;
            }
        }

        private void CompletarDatosPaciente(Paciente paciente)
        {
            this.LblId.Text = Convert.ToString(paciente.Id);
            this.TxtDni.Text = Convert.ToString(paciente.Dni);
            this.TxtNombre.Text = paciente.Nombre;
            this.TxtApellido.Text = paciente.Apellido;
        }

        private void BtnNuevoPaciente_Click(object sender, EventArgs e)
        {
            ResultadoOperacion resultadoOperacion = new ResultadoOperacion();
            if ((TxtDni.Text != "") && (TxtNombre.Text != "") && (TxtApellido.Text != ""))
            {
                Paciente paciente = new Paciente();

                paciente.Dni = Convert.ToInt32(TxtDni.Text);
                paciente.Nombre = TxtNombre.Text;
                paciente.Apellido = TxtApellido.Text;

                IFormPrincipal formPrincipal = this.Owner as IFormPrincipal;
                if (formPrincipal != null)
                {
                    resultadoOperacion = formPrincipal.NuevoPaciente(paciente);
                }
                else
                {
                    IFormGrilla formGrilla = this.Owner as IFormGrilla;
                    if (formGrilla != null) {
                        if (LblId.Text != "")
                        {
                            paciente.Id = Convert.ToInt32(LblId.Text);
                            resultadoOperacion = formGrilla.ModificacionPaciente(paciente, false);
                        }
                        else
                        {
                            resultadoOperacion = formGrilla.NuevoPaciente(paciente);
                        }
                    }
                }

                MessageBox.Show(resultadoOperacion.Resultado == true ? "La operación se realizó con exito" : resultadoOperacion.Mensaje);
                this.Close();
            }
            else
            {
                MessageBox.Show("Complete todos los datos necesarios");
            }
        }

        private void BtnCancelar_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
