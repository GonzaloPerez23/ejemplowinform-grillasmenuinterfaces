﻿using Logica;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProgramacionWinformGrillas
{
    interface IFormPrincipal
    {
        List<Paciente> ObtenerPacientes(int? id);

        ResultadoOperacion NuevoPaciente(Paciente paciente);

        ResultadoOperacion ModificacionPaciente(Paciente paciente, bool eliminar);
    }
}
