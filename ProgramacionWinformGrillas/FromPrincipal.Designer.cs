﻿namespace ProgramacionWinformGrillas
{
    partial class FormPrincipal
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.pACIENTESToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.lISTADODEPACIENTESToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.nUEVOCLIENTEToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pERSONALToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cONFIGURACIÓNToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.pACIENTESToolStripMenuItem,
            this.pERSONALToolStripMenuItem,
            this.cONFIGURACIÓNToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(389, 24);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // pACIENTESToolStripMenuItem
            // 
            this.pACIENTESToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.lISTADODEPACIENTESToolStripMenuItem,
            this.nUEVOCLIENTEToolStripMenuItem});
            this.pACIENTESToolStripMenuItem.Name = "pACIENTESToolStripMenuItem";
            this.pACIENTESToolStripMenuItem.Size = new System.Drawing.Size(78, 20);
            this.pACIENTESToolStripMenuItem.Text = "PACIENTES";
            // 
            // lISTADODEPACIENTESToolStripMenuItem
            // 
            this.lISTADODEPACIENTESToolStripMenuItem.Name = "lISTADODEPACIENTESToolStripMenuItem";
            this.lISTADODEPACIENTESToolStripMenuItem.Size = new System.Drawing.Size(199, 22);
            this.lISTADODEPACIENTESToolStripMenuItem.Text = "LISTADO DE PACIENTES";
            this.lISTADODEPACIENTESToolStripMenuItem.Click += new System.EventHandler(this.lISTADODEPACIENTESToolStripMenuItem_Click);
            // 
            // nUEVOCLIENTEToolStripMenuItem
            // 
            this.nUEVOCLIENTEToolStripMenuItem.Name = "nUEVOCLIENTEToolStripMenuItem";
            this.nUEVOCLIENTEToolStripMenuItem.Size = new System.Drawing.Size(199, 22);
            this.nUEVOCLIENTEToolStripMenuItem.Text = "NUEVO CLIENTE";
            this.nUEVOCLIENTEToolStripMenuItem.Click += new System.EventHandler(this.nUEVOCLIENTEToolStripMenuItem_Click);
            // 
            // pERSONALToolStripMenuItem
            // 
            this.pERSONALToolStripMenuItem.Name = "pERSONALToolStripMenuItem";
            this.pERSONALToolStripMenuItem.Size = new System.Drawing.Size(77, 20);
            this.pERSONALToolStripMenuItem.Text = "PERSONAL";
            // 
            // cONFIGURACIÓNToolStripMenuItem
            // 
            this.cONFIGURACIÓNToolStripMenuItem.Name = "cONFIGURACIÓNToolStripMenuItem";
            this.cONFIGURACIÓNToolStripMenuItem.Size = new System.Drawing.Size(114, 20);
            this.cONFIGURACIÓNToolStripMenuItem.Text = "CONFIGURACIÓN";
            // 
            // FormPrincipal
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(389, 185);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "FormPrincipal";
            this.Text = "Formulario principal";
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem pACIENTESToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem lISTADODEPACIENTESToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem nUEVOCLIENTEToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem pERSONALToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem cONFIGURACIÓNToolStripMenuItem;
    }
}