﻿namespace ProgramacionWinformGrillas
{
    partial class FormEjemploMenu
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.opción1ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.opción11ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.opción12ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.opción121ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.opcToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.opción2ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.opción3ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.opción1ToolStripMenuItem,
            this.opción2ToolStripMenuItem,
            this.opción3ToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(800, 24);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // opción1ToolStripMenuItem
            // 
            this.opción1ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.opción11ToolStripMenuItem,
            this.opción12ToolStripMenuItem});
            this.opción1ToolStripMenuItem.Name = "opción1ToolStripMenuItem";
            this.opción1ToolStripMenuItem.Size = new System.Drawing.Size(67, 20);
            this.opción1ToolStripMenuItem.Text = "Opción 1";
            // 
            // opción11ToolStripMenuItem
            // 
            this.opción11ToolStripMenuItem.Name = "opción11ToolStripMenuItem";
            this.opción11ToolStripMenuItem.Size = new System.Drawing.Size(131, 22);
            this.opción11ToolStripMenuItem.Text = "Opción 1.1";
            // 
            // opción12ToolStripMenuItem
            // 
            this.opción12ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.opción121ToolStripMenuItem,
            this.opcToolStripMenuItem});
            this.opción12ToolStripMenuItem.Name = "opción12ToolStripMenuItem";
            this.opción12ToolStripMenuItem.Size = new System.Drawing.Size(131, 22);
            this.opción12ToolStripMenuItem.Text = "Opción 1.2";
            // 
            // opción121ToolStripMenuItem
            // 
            this.opción121ToolStripMenuItem.Name = "opción121ToolStripMenuItem";
            this.opción121ToolStripMenuItem.Size = new System.Drawing.Size(140, 22);
            this.opción121ToolStripMenuItem.Text = "Opción 1.2.1";
            // 
            // opcToolStripMenuItem
            // 
            this.opcToolStripMenuItem.Name = "opcToolStripMenuItem";
            this.opcToolStripMenuItem.Size = new System.Drawing.Size(140, 22);
            this.opcToolStripMenuItem.Text = "Opc";
            // 
            // opción2ToolStripMenuItem
            // 
            this.opción2ToolStripMenuItem.Name = "opción2ToolStripMenuItem";
            this.opción2ToolStripMenuItem.Size = new System.Drawing.Size(67, 20);
            this.opción2ToolStripMenuItem.Text = "Opción 2";
            // 
            // opción3ToolStripMenuItem
            // 
            this.opción3ToolStripMenuItem.Name = "opción3ToolStripMenuItem";
            this.opción3ToolStripMenuItem.Size = new System.Drawing.Size(67, 20);
            this.opción3ToolStripMenuItem.Text = "Opción 3";
            // 
            // FormEjemploMenu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "FormEjemploMenu";
            this.Text = "FormEjemploMenu";
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem opción1ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem opción11ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem opción12ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem opción121ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem opcToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem opción2ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem opción3ToolStripMenuItem;
    }
}