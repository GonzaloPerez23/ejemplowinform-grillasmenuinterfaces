﻿namespace ProgramacionWinformGrillas
{
    partial class FormNuevoPaciente
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.BtnNuevoPaciente = new System.Windows.Forms.Button();
            this.TxtDni = new System.Windows.Forms.TextBox();
            this.TxtNombre = new System.Windows.Forms.TextBox();
            this.TxtApellido = new System.Windows.Forms.TextBox();
            this.LblDni = new System.Windows.Forms.Label();
            this.LblNombre = new System.Windows.Forms.Label();
            this.LblApellido = new System.Windows.Forms.Label();
            this.BtnCancelar = new System.Windows.Forms.Button();
            this.LblTitulo = new System.Windows.Forms.Label();
            this.LblIdTitle = new System.Windows.Forms.Label();
            this.LblId = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // BtnNuevoPaciente
            // 
            this.BtnNuevoPaciente.Location = new System.Drawing.Point(38, 169);
            this.BtnNuevoPaciente.Name = "BtnNuevoPaciente";
            this.BtnNuevoPaciente.Size = new System.Drawing.Size(55, 23);
            this.BtnNuevoPaciente.TabIndex = 0;
            this.BtnNuevoPaciente.Text = "Guardar";
            this.BtnNuevoPaciente.UseVisualStyleBackColor = true;
            this.BtnNuevoPaciente.Click += new System.EventHandler(this.BtnNuevoPaciente_Click);
            // 
            // TxtDni
            // 
            this.TxtDni.Location = new System.Drawing.Point(116, 63);
            this.TxtDni.Name = "TxtDni";
            this.TxtDni.Size = new System.Drawing.Size(100, 20);
            this.TxtDni.TabIndex = 1;
            // 
            // TxtNombre
            // 
            this.TxtNombre.Location = new System.Drawing.Point(116, 89);
            this.TxtNombre.Name = "TxtNombre";
            this.TxtNombre.Size = new System.Drawing.Size(100, 20);
            this.TxtNombre.TabIndex = 2;
            // 
            // TxtApellido
            // 
            this.TxtApellido.Location = new System.Drawing.Point(116, 115);
            this.TxtApellido.Name = "TxtApellido";
            this.TxtApellido.Size = new System.Drawing.Size(100, 20);
            this.TxtApellido.TabIndex = 3;
            // 
            // LblDni
            // 
            this.LblDni.AutoSize = true;
            this.LblDni.Location = new System.Drawing.Point(49, 63);
            this.LblDni.Name = "LblDni";
            this.LblDni.Size = new System.Drawing.Size(23, 13);
            this.LblDni.TabIndex = 4;
            this.LblDni.Text = "Dni";
            // 
            // LblNombre
            // 
            this.LblNombre.AutoSize = true;
            this.LblNombre.Location = new System.Drawing.Point(49, 92);
            this.LblNombre.Name = "LblNombre";
            this.LblNombre.Size = new System.Drawing.Size(44, 13);
            this.LblNombre.TabIndex = 5;
            this.LblNombre.Text = "Nombre";
            // 
            // LblApellido
            // 
            this.LblApellido.AutoSize = true;
            this.LblApellido.Location = new System.Drawing.Point(49, 118);
            this.LblApellido.Name = "LblApellido";
            this.LblApellido.Size = new System.Drawing.Size(44, 13);
            this.LblApellido.TabIndex = 6;
            this.LblApellido.Text = "Apellido";
            // 
            // BtnCancelar
            // 
            this.BtnCancelar.Location = new System.Drawing.Point(158, 169);
            this.BtnCancelar.Name = "BtnCancelar";
            this.BtnCancelar.Size = new System.Drawing.Size(58, 23);
            this.BtnCancelar.TabIndex = 7;
            this.BtnCancelar.Text = "Cancelar";
            this.BtnCancelar.UseVisualStyleBackColor = true;
            this.BtnCancelar.Click += new System.EventHandler(this.BtnCancelar_Click);
            // 
            // LblTitulo
            // 
            this.LblTitulo.AutoSize = true;
            this.LblTitulo.Location = new System.Drawing.Point(68, 9);
            this.LblTitulo.Name = "LblTitulo";
            this.LblTitulo.Size = new System.Drawing.Size(101, 13);
            this.LblTitulo.TabIndex = 8;
            this.LblTitulo.Text = "NUEVO PACIENTE";
            // 
            // LblIdTitle
            // 
            this.LblIdTitle.AutoSize = true;
            this.LblIdTitle.Location = new System.Drawing.Point(49, 37);
            this.LblIdTitle.Name = "LblIdTitle";
            this.LblIdTitle.Size = new System.Drawing.Size(16, 13);
            this.LblIdTitle.TabIndex = 9;
            this.LblIdTitle.Text = "Id";
            // 
            // LblId
            // 
            this.LblId.AutoSize = true;
            this.LblId.Location = new System.Drawing.Point(113, 37);
            this.LblId.Name = "LblId";
            this.LblId.Size = new System.Drawing.Size(58, 13);
            this.LblId.TabIndex = 10;
            this.LblId.Text = "IdPaciente";
            // 
            // FormNuevoPaciente
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(255, 204);
            this.Controls.Add(this.LblId);
            this.Controls.Add(this.LblIdTitle);
            this.Controls.Add(this.LblTitulo);
            this.Controls.Add(this.BtnCancelar);
            this.Controls.Add(this.LblApellido);
            this.Controls.Add(this.LblNombre);
            this.Controls.Add(this.LblDni);
            this.Controls.Add(this.TxtApellido);
            this.Controls.Add(this.TxtNombre);
            this.Controls.Add(this.TxtDni);
            this.Controls.Add(this.BtnNuevoPaciente);
            this.Name = "FormNuevoPaciente";
            this.Text = "Nuevo paciente";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button BtnNuevoPaciente;
        private System.Windows.Forms.TextBox TxtDni;
        private System.Windows.Forms.TextBox TxtNombre;
        private System.Windows.Forms.TextBox TxtApellido;
        private System.Windows.Forms.Label LblDni;
        private System.Windows.Forms.Label LblNombre;
        private System.Windows.Forms.Label LblApellido;
        private System.Windows.Forms.Button BtnCancelar;
        private System.Windows.Forms.Label LblTitulo;
        private System.Windows.Forms.Label LblIdTitle;
        private System.Windows.Forms.Label LblId;
    }
}