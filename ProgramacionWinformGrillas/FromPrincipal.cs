﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Logica;

namespace ProgramacionWinformGrillas
{
    public partial class FormPrincipal : Form, IFormPrincipal
    {
        private Hospital Hospital { get; set; }

        public FormPrincipal()
        {
            Hospital = new Hospital();

            InitializeComponent();
        }

        public ResultadoOperacion NuevoPaciente(Paciente paciente)
        {
            return Hospital.NuevoPaciente(paciente);
        }

        public ResultadoOperacion ModificacionPaciente(Paciente paciente, bool eliminar)
        {
            return Hospital.ModificacionPaciente(paciente, eliminar);
        }

        public List<Paciente> ObtenerPacientes(int? id)
        {
            return Hospital.ObtenerPacientes(id);
        }

        private void lISTADODEPACIENTESToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FormGrilla form = new FormGrilla();
            form.Owner = this;
            form.ShowDialog();
        }

        private void nUEVOCLIENTEToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FormNuevoPaciente form = new FormNuevoPaciente(new Paciente());
            form.Owner = this;
            form.ShowDialog();
        }
    }
}
