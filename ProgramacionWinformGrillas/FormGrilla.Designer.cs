﻿namespace ProgramacionWinformGrillas
{
    partial class FormGrilla
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.GridPacientes = new System.Windows.Forms.DataGridView();
            this.LblTitulo = new System.Windows.Forms.Label();
            this.BtnFormNuevoPaciente = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.GridPacientes)).BeginInit();
            this.SuspendLayout();
            // 
            // GridPacientes
            // 
            this.GridPacientes.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.GridPacientes.Location = new System.Drawing.Point(42, 61);
            this.GridPacientes.Name = "GridPacientes";
            this.GridPacientes.Size = new System.Drawing.Size(421, 150);
            this.GridPacientes.TabIndex = 1;
            this.GridPacientes.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.GridPacientes_CellContentClick);
            // 
            // LblTitulo
            // 
            this.LblTitulo.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblTitulo.Location = new System.Drawing.Point(37, 16);
            this.LblTitulo.Name = "LblTitulo";
            this.LblTitulo.Size = new System.Drawing.Size(274, 33);
            this.LblTitulo.TabIndex = 0;
            this.LblTitulo.Text = "Listado de pacientes";
            // 
            // BtnFormNuevoPaciente
            // 
            this.BtnFormNuevoPaciente.Location = new System.Drawing.Point(367, 24);
            this.BtnFormNuevoPaciente.Name = "BtnFormNuevoPaciente";
            this.BtnFormNuevoPaciente.Size = new System.Drawing.Size(97, 23);
            this.BtnFormNuevoPaciente.TabIndex = 0;
            this.BtnFormNuevoPaciente.Text = "Nuevo paciente";
            this.BtnFormNuevoPaciente.Click += new System.EventHandler(this.BtnFormNuevoPaciente_Click);
            // 
            // FormGrilla
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(493, 240);
            this.Controls.Add(this.BtnFormNuevoPaciente);
            this.Controls.Add(this.LblTitulo);
            this.Controls.Add(this.GridPacientes);
            this.Name = "FormGrilla";
            this.Text = "Formulario Principal";
            this.Load += new System.EventHandler(this.FormGrilla_Load);
            ((System.ComponentModel.ISupportInitialize)(this.GridPacientes)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView GridPacientes;
        private System.Windows.Forms.Label LblTitulo;
        private System.Windows.Forms.Button BtnFormNuevoPaciente;
    }
}

