﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Logica;

namespace ProgramacionWinformGrillas
{
    public partial class FormGrilla : Form, IFormGrilla
    {
        public FormGrilla()
        {
            InitializeComponent();
        }

        private void ActualizarGridPacientes() {
            IFormPrincipal formPaciente = this.Owner as IFormPrincipal;

            if (formPaciente != null)
            {
                this.GridPacientes.DataSource = formPaciente.ObtenerPacientes(null);
            }
        }

        private void BtnFormNuevoPaciente_Click(object sender, EventArgs e)
        {
            FormNuevoPaciente formNuevoPaciente = new FormNuevoPaciente(new Paciente());
            formNuevoPaciente.Owner = this;
            formNuevoPaciente.Show();
        }

        public ResultadoOperacion NuevoPaciente(Paciente paciente)
        {
            ResultadoOperacion resultadoOperacion = new ResultadoOperacion(false, "Error interno en el servidor");
            IFormPrincipal formPrincipal = this.Owner as IFormPrincipal;
            if (formPrincipal != null) {
                resultadoOperacion = formPrincipal.NuevoPaciente(paciente);

                if (resultadoOperacion.Resultado) {
                    ActualizarGridPacientes();
                }
            }

            return resultadoOperacion;
        }

        public ResultadoOperacion ModificacionPaciente(Paciente paciente, bool Eliminar)
        {
            ResultadoOperacion resultadoOperacion = new ResultadoOperacion(false, "Error interno en el servidor");
            IFormPrincipal formPrincipal = this.Owner as IFormPrincipal;
            if (formPrincipal != null)
            {
                resultadoOperacion = formPrincipal.ModificacionPaciente(paciente, Eliminar);

                if (resultadoOperacion.Resultado)
                {
                    ActualizarGridPacientes();
                }
            }

            return resultadoOperacion;
        }

        private void GridPacientes_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            var column = this.GridPacientes.Columns[e.ColumnIndex];
            var row = this.GridPacientes.Rows[e.RowIndex];

            if (column.HeaderText == "Eliminar")
            {
                var mensaje = MessageBox.Show("¿Está seguro que desea Eliminar el cliente?", "Eliminar cliente", MessageBoxButtons.OKCancel);

                if (mensaje == DialogResult.OK)
                {
                    Paciente paciente = row.DataBoundItem as Paciente;
                    IFormPrincipal formPrincipal = this.Owner as IFormPrincipal;

                    if (formPrincipal != null)
                    {
                        formPrincipal.ModificacionPaciente(paciente, true);
                        ActualizarGridPacientes();
                    }
                }
            }
            else
            {
                if (column.HeaderText == "Edicion")
                {
                    Paciente paciente = row.DataBoundItem as Paciente;
                    FormNuevoPaciente formNuevoPaciente = new FormNuevoPaciente(paciente);
                    formNuevoPaciente.Owner = this;
                    formNuevoPaciente.ShowDialog();
                }
            }
        }

        private void FormGrilla_Load(object sender, EventArgs e)
        {
            //this.GridPacientes.AutoGenerateColumns = false;
            //this.GridPacientes.ColumnCount = 3;

            //this.GridPacientes.Columns[0].HeaderText = "ID";
            //this.GridPacientes.Columns[0].DataPropertyName = "Id";
            //this.GridPacientes.Columns[1].HeaderText = "DNI";
            //this.GridPacientes.Columns[1].DataPropertyName = "Dni";
            //this.GridPacientes.Columns[2].HeaderText = "NOMBRE";
            //this.GridPacientes.Columns[2].DataPropertyName = "Nombre";

            IFormPrincipal owner = this.Owner as IFormPrincipal;
            this.GridPacientes.AutoGenerateColumns = true;
            if (owner != null)
            {
                GridPacientes.DataSource = owner.ObtenerPacientes(null);
            }

            DataGridViewLinkColumn editar = new DataGridViewLinkColumn();
            editar.HeaderText = "Edicion";
            editar.Text = "Editar";
            editar.UseColumnTextForLinkValue = true;
            GridPacientes.Columns.Add(editar);

            DataGridViewLinkColumn eliminar = new DataGridViewLinkColumn();
            eliminar.HeaderText = "Eliminar";
            eliminar.Text = "Eliminar";
            eliminar.UseColumnTextForLinkValue = true;
            GridPacientes.Columns.Add(eliminar);
        }
    }
}
