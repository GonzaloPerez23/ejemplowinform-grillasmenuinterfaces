﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logica
{
    public class Paciente
    {
        public Paciente()
        {

        }

        public Paciente(int id, int dni, string nombre, string apellido)
        {
            this.Id = id;
            this.Dni = dni;
            this.Nombre = nombre;
            this.Apellido = apellido;
        }

        public int Id { get; set; }

        public int Dni { get; set; }

        public string Nombre { get; set; }

        public string Apellido { get; set; }
    }
}
