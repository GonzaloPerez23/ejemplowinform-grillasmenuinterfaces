﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logica
{
    public class Hospital
    {
        private List<Paciente> pacientes { get; set; }

        public Hospital()
        {
            pacientes = new List<Paciente>();
        }

        public List<Paciente> ObtenerPacientes(int? id) {
            return pacientes.Where(x => id.HasValue ? x.Id == id : true).ToList();
        }

        public ResultadoOperacion NuevoPaciente(Paciente nuevoPaciente)
        {
            ResultadoOperacion resultado = new ResultadoOperacion();

            nuevoPaciente.Id = pacientes.Count() + 1;
            pacientes.Add(nuevoPaciente);

            return resultado;
        }

        public ResultadoOperacion ModificacionPaciente(Paciente nuevoPaciente, bool eliminar)
        {
            ResultadoOperacion resultado = new ResultadoOperacion();

            if (!eliminar) //Modificación de paciente
            {
                Paciente paciente = pacientes.FirstOrDefault(x => x.Id == nuevoPaciente.Id);
                paciente.Nombre = nuevoPaciente.Nombre;
                paciente.Apellido = nuevoPaciente.Apellido;
                paciente.Dni = nuevoPaciente.Dni;
            }
            else //Eliminación de paciente
            {
                pacientes.RemoveAll(x => x.Id == nuevoPaciente.Id);
            }

            return resultado;
        }
    }
}
