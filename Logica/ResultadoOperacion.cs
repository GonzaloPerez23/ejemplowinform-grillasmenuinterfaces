﻿namespace Logica
{
    public class ResultadoOperacion
    {
        public ResultadoOperacion()
        {
            this.Resultado = true;
        }
        
        public ResultadoOperacion(bool resultado, string mensaje) {
            this.Resultado = resultado;
            this.Mensaje = mensaje;

        }

        public bool Resultado { get; set; }

        public string Mensaje { get; set; }
    }
}